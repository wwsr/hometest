//
//  HomeTestTests.swift
//  HomeTestTests
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import XCTest
@testable import HomeTest

class HomeTestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCheckLocationsLoading() {
        Network.shared.getLocationsData { (locationData) in
            XCTAssert(locationData?.locations != nil)
            XCTAssert(locationData?.updated != nil)
        }
    }
    
    func testChangingLocation() {
        MemoryManager.shared.loadLocationData()
        let locations = MemoryManager.shared.locationsData?.locations
        let editingLocation = RealmLocation(lng: locations!.first!.lng, lat: locations!.first!.lat, name: "New Name", note: "New Note")
        
        MemoryManager.shared.changeRealmLocationData(editingLocation: editingLocation)
        
        let newLocations = MemoryManager.shared.locationsData?.locations
        
        XCTAssert(newLocations?.first != locations?.first)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
