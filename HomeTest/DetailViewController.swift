//
//  DetailViewController.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var notesTextView: UITextView!
    
    var editingLocation: RealmLocation?
    
    weak var delegate: MapUpdateProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let location = editingLocation else {
            return
        }
        _ = location.lng
        _ = location.lat
        let name = location.name
        let note = location.note
        
        nameTextField.text = name
        notesTextView.text = note
    }
    
    @IBAction func doneButtonPressed() {
        guard let editingLocation = editingLocation else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        editingLocation.name = nameTextField.text ?? ""
        editingLocation.note = notesTextView.text ?? ""
        
        MemoryManager.shared.changeRealmLocationData(editingLocation: editingLocation)
        
        delegate?.updateLocations()
        
        
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
