//
//  ViewController.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Contacts

class ViewController: UIViewController, UIGestureRecognizerDelegate, CLLocationManagerDelegate, MapUpdateProtocol {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 1000
    var myLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        self.getDataFromServer()
        
        mapView.delegate = self
        
        addGestureRec()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    func addGestureRec() {
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    func showStateText(_ text: String) {
        print(text)
    }
    
    func loadDataFromMemory() {
        MemoryManager.shared.loadLocationData()
    }
    
    func getDataFromServer() {
        Network.shared.getLocationsData { result in
//            print(result)
            guard let result = result else {
                return
            }
            guard result.locations != nil else {
                return
            }
            guard let updated = result.updated else {
                return
            }
            if (!self.isDataIsActual(updated)) {
                self.updateData(result)
            }
            self.showStateText("Map")
            self.showMarkers()
        }
    }
    
    func isDataIsActual(_ updated: String) -> Bool {
        guard let lastUpdated = MemoryManager.shared.locationsData?.updated else {
            return false
        }
        return (lastUpdated == updated)
    }
    
    func updateData(_ data: LocationData) {
        print("updateData")
        MemoryManager.shared.changeRealmLocationData(locationData: data)
    }
    
    func showMarkers() {
        print("ShowMarkers")
        guard let locationsData = MemoryManager.shared.locationsData else {
            return
        }
        let locations = locationsData.locations
        
        if let first = locations.first {
            let initialLocation = CLLocation(latitude: first.lat, longitude: first.lng)
            centerMapOnLocation(location: initialLocation)
        }
        
        for location in locations {
            let lat = location.lat
            let long = location.lng
            let name = location.name
            let note = location.note
            
            let artwork = Artwork(title: name,
                                  locationName: note,
                                  discipline: "",
                                  coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
            mapView.addAnnotation(artwork)
        }
    }
    
    func updateLocations() {
        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)
        showMarkers()
    }
    
    @objc func handleTap(_ gestureReconizer: UILongPressGestureRecognizer) {
        
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)

        let artwork = Artwork(title: "Unknown Location",
                              locationName: "",
                              discipline: "",
                              coordinate: coordinate)
        mapView.addAnnotation(artwork)
        
        let realmLocation = RealmLocation(lng: Double(coordinate.longitude), lat: Double(coordinate.latitude), name: "Unknown Location", note: "")
        MemoryManager.shared.addNewLocation(realmLocation: realmLocation)
    }
    
    let locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        
        
//        // Ask for Authorisation from the User.
//        self.locationManager.requestAlwaysAuthorization()
//
//        // For use in foreground
//        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestAlwaysAuthorization()
        }
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            return
        }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        myLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    @IBAction func listButtonPressed(_ sender: Any) {
        guard let locationsData = MemoryManager.shared.locationsData else {
            return
        }
        let locations = locationsData.locations
        
        
        var sortablePlaces = Array<Place>()
        for location in locations {
            let place = Place()
            place.latitude = location.lat
            place.longitude = location.lng
            place.name = location.name
            place.note = location.note
            sortablePlaces.append(place)
        }
        if let myLocation = myLocation {
            sortablePlaces.sort(by: { $0.distance(to: myLocation) < $1.distance(to: myLocation) })
        } else {
            let firstPlaceCLLocation = CLLocation(latitude: sortablePlaces[0].latitude, longitude: sortablePlaces[0].longitude)
            sortablePlaces.sort(by: { $0.distance(to: firstPlaceCLLocation) < $1.distance(to: firstPlaceCLLocation) })
        }
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let listViewController = storyBoard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
        listViewController.sortablePlaces = sortablePlaces
        self.navigationController?.pushViewController(listViewController, animated: true)
    }
    
    
    @IBAction func removeButtonPressed(_ sender: Any) {
        MemoryManager.shared.removeMotionData()
    }
    
}

protocol MapUpdateProtocol: class {
    func updateLocations()
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let realmLoc = RealmLocation(lng: location.coordinate.longitude, lat: location.coordinate.latitude, name: location.title ?? "", note: location.locationName)
        detailViewController.editingLocation = realmLoc
        detailViewController.delegate = self
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Artwork else {
            return nil
        }
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}


