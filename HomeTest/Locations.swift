//
//  Locations.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import Foundation
import ObjectMapper

struct Location: Mappable {
    var lng: Double?
    var lat: Double?
    var name: String?
    var note: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        name     <- map["name"]
        lat      <- map["lat"]
        lng      <- map["lng"]
    }
}
