//
//  Place.swift
//  HomeTest
//
//  Created by Артем on 11/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import Foundation
import CoreLocation

class Place {
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    var name: String!
    var note: String!
    
    var location: CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
    
    func distance(to location: CLLocation) -> CLLocationDistance {
        return location.distance(from: self.location)
    }
}
