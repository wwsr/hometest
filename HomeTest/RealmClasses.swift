//
//  RealmClasses.swift
//  HomeTest
//
//  Created by Артем on 11/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import Foundation
import RealmSwift

class RealmLocationData: Object {
    var locations: List<RealmLocation> = List<RealmLocation>()
    @objc dynamic var updated: String = ""
    
    convenience init(locations:List<RealmLocation>, updated: String) {
        self.init()
        self.locations = locations
        self.updated = updated
    }
}

class RealmLocation: Object {
    @objc dynamic var lng: Double = 0.0
    @objc dynamic var lat: Double = 0.0
    @objc dynamic var name: String = ""
    @objc dynamic var note: String = ""
    
    convenience init(lng: Double, lat: Double, name: String, note: String) {
        self.init()
        self.lng = lng
        self.lat = lat
        self.name = name
        self.note = note
    }
}
