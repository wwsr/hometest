//
//  Network.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class Network {
    static let shared = Network()
    
    private init() {
    }
    
    func getURLString() -> String {
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist"),
            let config = NSDictionary(contentsOfFile: path){
            if let locationsDataUrl = config["locationDataUrl"] as? String {
                return locationsDataUrl
            }
        }
        return ""
    }
    
    func loadJsonFromFile() -> Dictionary<String, Any>? {
        if let path = Bundle.main.path(forResource: "locations", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print(jsonResult)
                print(type(of:jsonResult))
                return jsonResult as? Dictionary<String, Any>
            } catch {
                print("Error")
            }
        }
        return nil
    }
    
    func getLocationsData(result:@escaping (LocationData?)->Void) {
        let urlString = getURLString()
//        guard let json = loadJsonFromFile() else {
//            return nil
//        }
//        guard let jsonLocations = json["locations"] as? [[String : Any]] else {
//            return nil
//        }
//        let locations = Mapper<Location>().mapArray(JSONArray: jsonLocations)
//        print(locations)
//        print(type(of: locations))
//        return locations
        
        Alamofire.request(urlString).responseJSON { response in
            switch response.result {
            case .success:
                if let json = response.result.value {
                    print("JSON: \(json)")
                    if let json = json as? Dictionary<String, Any> {
                        guard let jsonLocations = json["locations"] as? [[String : Any]] else {
                            return
                        }
                        let locationsResult = Mapper<Location>().mapArray(JSONArray: jsonLocations)
                        print(locationsResult)
                        let locationData = LocationData()
                        var locations: Array<Location>?
                        locations = locationsResult
                        locationData.updated = json["updated"] as? String
                        locationData.locations = locations
                        result(locationData)
                    }
                }
                break
            case .failure(let error):
                print(error)
                result(nil)
            }
        }
    }
    
    
}

class LocationData {
    var locations: Array<Location>?
    var updated: String?
}
