//
//  ListTableViewCell.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var lng: UILabel!
    @IBOutlet weak var lat: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLocationsData(place: Place) {
        name.text = place.name
        note.text = place.note
        lng.text = String(place.longitude)
        lat.text = String(place.latitude)
    }

}
