//
//  MemoryManager.swift
//  HomeTest
//
//  Created by Артем on 08/10/2018.
//  Copyright © 2018 epam. All rights reserved.
//

import Foundation
import RealmSwift

class MemoryManager {
    static let shared: MemoryManager = MemoryManager()
    var locationsData: RealmLocationData?
    
    private let realm = try! Realm()
    private init() {
        loadLocationData()
    }
    
    func saveLocationData(locationData: RealmLocationData) {
        
        try! realm.write {
            realm.add(locationData)
        }
    }
    
    func loadLocationData() {
        locationsData = realm.objects(RealmLocationData.self).first
//        print(locationsData)
    }
    
    func removeMotionData() {
        try! realm.write {
            realm.deleteAll()
        }
    }

    func changeRealmLocationData(locationData: LocationData) {
        guard let locations = locationData.locations else {
            return
        }
        guard let updated = locationData.updated else {
            return
        }
        let realmLocations = List<RealmLocation>()
        for location in locations {
            let realmLocation = RealmLocation(lng: location.lng ?? 0.0, lat: location.lat  ?? 0.0, name: location.name ?? "", note: "")
            realmLocations.append(realmLocation)
        }
        let realmLocationData = RealmLocationData(locations: realmLocations, updated: updated)

        self.saveLocationData(locationData: realmLocationData)
        self.loadLocationData()
    }
    
    func addNewLocation(realmLocation: RealmLocation) {
        loadLocationData()
        if let locationsData = locationsData {
            let realmLocations = locationsData.locations
            
            if let curLoc = realmLocations.filter("lat == \(Double(realmLocation.lat)) && lng == \(Double(realmLocation.lng))").first {
                return
            }
            
            try! self.realm.write {
                realmLocations.append(realmLocation)
                loadLocationData()
            }
            return
        }
        let realmLocations = List<RealmLocation>()
        let dataToSave = RealmLocationData(locations: realmLocations, updated: "updated")
        try! self.realm.write {
            realm.add(dataToSave)
        }
    }
    
    func changeRealmLocationData(editingLocation: RealmLocation) {
        loadLocationData()
        if let locationsData = locationsData {
            let realmLocations = locationsData.locations
                guard let curLoc = realmLocations.filter("lat == \(Double(editingLocation.lat)) && lng == \(Double(editingLocation.lng))").first else {
                    return
                }
                try! self.realm.write {
                    curLoc.name = editingLocation.name
                    curLoc.note = editingLocation.note
                }
                self.loadLocationData()
        }
    }
}



